package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Operaciones op;
    private EditText nombre;
    private Button btnEntrar, btnSalir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nombre=(EditText) findViewById(R.id.textNombre);
        btnEntrar =(Button) findViewById(R.id.btnEntrar);
        btnSalir =(Button) findViewById(R.id.btnSalir);




        op = new Operaciones();

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Cliente = nombre.getText().toString();
                if (Cliente.matches("")){
                    Toast.makeText(MainActivity.this, "Falto el nombre", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(MainActivity.this, RectanguloAct.class);
                    intent.putExtra("Cliente", Cliente);
                    startActivity(intent);
                }
            }
        });


        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });






    }
}
