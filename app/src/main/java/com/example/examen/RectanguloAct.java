package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RectanguloAct extends AppCompatActivity {
    private Operaciones op;
    private TextView lblCliente;
    private Button btnCalcular, btnLimpiar, btnRegresar;

    private EditText textAltura, textBase;

    private TextView lblResultadoArea, lblResultadoPerimetro;








    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);


        lblCliente =(TextView) findViewById(R.id.lblNombre);

        btnCalcular = (Button) findViewById(R.id.btnCalc);
        btnRegresar = (Button) findViewById(R.id.btnReg);
        btnLimpiar = (Button) findViewById(R.id.btnLimp);

        textAltura = (EditText) findViewById(R.id.txtAltura);
        textBase = (EditText) findViewById(R.id.txtBase);

        lblResultadoArea = (TextView) findViewById(R.id.lblArea);
        lblResultadoPerimetro = (TextView) findViewById(R.id.lblPerimetro);

        op = new Operaciones();

        Bundle datos = getIntent().getExtras();
        lblCliente.setText(datos.getString("Cliente"));


        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String base = textBase.getText().toString();
                String altura = textAltura.getText().toString();

                if(base.matches("")||altura.matches(""))
                {
                    Toast.makeText(RectanguloAct.this, "Rellena todos los campos", Toast.LENGTH_SHORT).show();
                }else{
                    int basee = Integer.valueOf(textBase.getText().toString());
                    int alturaa = Integer.valueOf(textAltura.getText().toString());

                    op.setAltura(alturaa);
                    op.setBase(basee);

                    lblResultadoArea.setText(""+op.calcularArea());
                    lblResultadoPerimetro.setText(""+op.calcularPerimetro());
                }







            }
        });


        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String base = textBase.getText().toString();
                String altura = textAltura.getText().toString();

                if(base.matches("")||altura.matches(""))
                {
                    Toast.makeText(RectanguloAct.this, "Ya se encuentra limpio", Toast.LENGTH_SHORT).show();
                }
                else{
                    textAltura.setText("");
                    textBase.setText("");
                    lblResultadoPerimetro.setText("");
                    lblResultadoArea.setText("");
                    Toast.makeText(RectanguloAct.this, "Se ah limpiado", Toast.LENGTH_SHORT).show();
                }

            }
        });


        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RectanguloAct.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
